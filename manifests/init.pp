class terraform (
  String $version,
  Stdlib::Absolutepath $bin_dir,
  Stdlib::Httpurl $download_url = 'https://releases.hashicorp.com/terraform/${version}/terraform_${version}_linux_amd64.zip'
) {
  class{'terraform::install': } ->
  Class['terraform']
}
